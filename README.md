# Prerequisites

* Kubernetes cluster
* kubectl (at least v1.11)
* helm
* curl
* golang (dev)
* docker (dev)
* make (dev)
* mongoimport (already built from [repo](https://github.com/mongodb/mongo-tools))

# Build

```
make all
```

# Run mongo

```
docker run --name mongo -ti --rm mongo
```

# Import data

```
docker run --name mongoimport -ti --rm kayrus/mongo:v1 bash -c "/mongoimport --type=csv --collection titanic --headerline --file <(curl -s https://gitlab.com/ContainerSolutions/API-Exercise/raw/master/titanic.csv) --host=$(docker inspect mongo --format '{{.NetworkSettings.IPAddress}}')"
```

# Run client

```
docker run --name apiserver -ti --rm -p 8080:8080 -e MONGO_URL="mongodb://$(docker inspect mongo --format '{{.NetworkSettings.IPAddress}}'):27017" kayrus/apiserver:v1
```

# curl

```
curl localhost:8080/people
```

# Deploy to Kubernetes

```
kubectl create -f sa.yaml
helm init --debug --service-account=tiller
helm install --name mychart mychart
```

# Import data

```
kubectl run import --rm -ti --image=kayrus/mongo:v1 --restart=Never -- bash -c "/mongoimport --type=csv --collection titanic --headerline --file <(curl -s https://gitlab.com/ContainerSolutions/API-Exercise/raw/master/titanic.csv) --host=mongo-v1"
```

# Destroy

```
helm del --purge mychart
```

# Upgarde

## Major

Change the [chart](mychart/Chart.yaml#L4) and [software version](mychart/values.yaml#L4) and run:

```
helm install --name mychart-v2 mychart
```

this will allow you to have two major software versions.

## Minor

Change the [software version](mychart/values.yaml#L4) and run:

```
helm upgrade mychart mychart
```

# Test

## Prepare

```
kubectl port-forward svc/apiserver-v1 8080:8080
```

## curl

```
curl localhost:8080/people
```
