package shared

import (
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"sync"
	"syscall"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/x/bsonx/bsoncore"
)

const logFormat = "%s [%s] \"%s\" %d %d %.10f\n"

type JsonDocument struct {
	sync.RWMutex
	ID              primitive.ObjectID `bson:"_id,omitempty" json:"uuid"`
	Survived        int                `bson:"Survived,omitempty" json:"-"`
	SurvivedBool    bool               `bson:"-" json:"survived"`
	Pclass          int                `bson:"Pclass,omitempty" json:"passengerClass"`
	Name            string             `bson:"Name,omitempty" json:"name"`
	Sex             string             `bson:"Sex,omitempty" json:"sex"`
	Age             float64            `bson:"Age,omitempty" json:"age"`
	SiblingsSpouses int                `bson:"Siblings/Spouses Aboard,omitempty" json:"siblingsOrSpousesAboard"`
	ParentsChildren int                `bson:"Parents/Children Aboard,omitempty" json:"parentsOrChildrenAboard"`
	Fare            float64            `bson:"Fare,omitempty" json:"fare"`
}

func (r JsonDocument) MarshalJSON() ([]byte, error) {
	r.SurvivedBool = r.Survived > 0
	type alias JsonDocument
	return json.Marshal(&struct{ alias }{alias: (alias)(r)})
}

func (r *JsonDocument) UnmarshalJSON(b []byte) error {
	type tmp JsonDocument
	var s tmp
	err := json.Unmarshal(b, &s)
	if err != nil {
		return err
	}
	*r = JsonDocument(s)

	if s.SurvivedBool {
		r.Survived = 1
	}

	return nil
}

func ToDoc(v interface{}) (doc *bsoncore.Document, err error) {
	data, err := bson.Marshal(v)
	if err != nil {
		return
	}

	err = bson.Unmarshal(data, &doc)
	return
}

type LogRecord struct {
	http.ResponseWriter

	Ip                    string
	Method, Uri, Protocol string
	Status                int
	Time                  time.Time
	ResponseBytes         int64
	ElapsedTime           time.Duration
}

func HandleSignal() {
	sigc := make(chan os.Signal, 1)
	signal.Notify(sigc,
		syscall.SIGHUP,
		syscall.SIGINT,
		syscall.SIGTERM,
		syscall.SIGQUIT)
	go func() {
		s := <-sigc
		fmt.Fprintf(os.Stderr, "Caught %d signal, exiting\n", s)
		os.Exit(0)
	}()
}

func NewLogRecord(w http.ResponseWriter, r *http.Request) *LogRecord {
	return &LogRecord{
		ResponseWriter: w,
		Ip:             r.RemoteAddr,
		Time:           time.Now(),
		Method:         r.Method,
		Uri:            r.RequestURI,
		Protocol:       r.Proto,
		Status:         http.StatusOK,
		ElapsedTime:    time.Duration(0),
		ResponseBytes:  0,
	}
}

func HealthHandler(w http.ResponseWriter, r *http.Request) {
	record := NewLogRecord(w, r)

	switch r.Method {
	case "GET":
		written, err := w.Write([]byte("ok"))
		if err != nil {
			fmt.Fprintf(os.Stderr, "%v\n", err)
		} else {
			record.ResponseBytes += int64(written)
		}
	default:
		w.WriteHeader(http.StatusMethodNotAllowed)
		record.Status = http.StatusMethodNotAllowed
		fmt.Fprintf(w, "Method is not allowed")
	}

	record.Log()
}

func (r *LogRecord) Log() {
	finishTime := time.Now()
	finishTimeT := finishTime.UTC()
	r.ElapsedTime = finishTime.Sub(r.Time)

	clientIP := r.Ip
	if colon := strings.LastIndex(clientIP, ":"); colon != -1 {
		clientIP = clientIP[:colon]
	}

	timeFormatted := finishTimeT.Format("02/Jan/2006 03:04:05")
	requestLine := fmt.Sprintf("%s %s %s", r.Method, r.Uri, r.Protocol)
	fmt.Printf(logFormat, clientIP, timeFormatted, requestLine, r.Status, r.ResponseBytes,
		r.ElapsedTime.Seconds())
}
