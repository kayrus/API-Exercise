package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
	"shared"
)

func docsHandler(collection *mongo.Collection) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		record := shared.NewLogRecord(w, r)
		defer record.Log()
		ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)

		var indent string
		switch r.Header.Get("Accept") {
		case "application/json":
			w.Header().Set("Content-Type", "application/json")
		default:
			w.Header().Set("Content-Type", "text/html")
			indent = "  "
		}

		switch r.Method {
		case "GET":
			var results []shared.JsonDocument

			cur, err := collection.Find(ctx, bson.D{})

			if err != nil {
				fmt.Fprintf(os.Stderr, "%v\n", err)
				return
			}

			defer cur.Close(ctx)
			for cur.Next(ctx) {
				var res shared.JsonDocument
				err := cur.Decode(&res)
				if err != nil {
					fmt.Fprintf(os.Stderr, "%v\n", err)
					return
				}
				results = append(results, res)
			}

			if err := cur.Err(); err != nil {
				fmt.Fprintf(os.Stderr, "%v\n", err)
				return
			}

			j, _ := json.MarshalIndent(results, "", indent)
			written, err := w.Write(j)
			if err != nil {
				fmt.Fprintf(os.Stderr, "%v\n", err)
			} else {
				record.ResponseBytes += int64(written)
			}
		case "POST":
			var tmp shared.JsonDocument
			err := json.NewDecoder(r.Body).Decode(&tmp)
			if err != nil {
				fmt.Fprintf(os.Stderr, "%v\n", err)
				w.WriteHeader(http.StatusInternalServerError)
				w.Write([]byte(fmt.Sprintf("%v\n", err)))
				record.Status = http.StatusInternalServerError
				return
			}

			tmp.ID = primitive.NewObjectID()
			doc, err := shared.ToDoc(tmp)
			if err != nil {
				fmt.Fprintf(os.Stderr, "%v\n", err)
				w.WriteHeader(http.StatusInternalServerError)
				w.Write([]byte(fmt.Sprintf("%v\n", err)))
				record.Status = http.StatusInternalServerError
				return
			}

			result, err := collection.InsertOne(ctx, doc)
			if err != nil {
				fmt.Fprintf(os.Stderr, "%v\n", err)
				w.WriteHeader(http.StatusInternalServerError)
				w.Write([]byte(fmt.Sprintf("%v\n", err)))
				record.Status = http.StatusInternalServerError
				return
			}

			j, _ := json.MarshalIndent(result, "", indent)
			w.WriteHeader(http.StatusCreated)
			record.Status = http.StatusCreated
			written, err := w.Write(j)
			if err != nil {
				fmt.Fprintf(os.Stderr, "%v\n", err)
				w.WriteHeader(http.StatusInternalServerError)
				w.Write([]byte(fmt.Sprintf("%v\n", err)))
				record.Status = http.StatusInternalServerError
			} else {
				record.ResponseBytes += int64(written)
			}
		default:
			w.WriteHeader(http.StatusMethodNotAllowed)
			record.Status = http.StatusMethodNotAllowed
			fmt.Fprintf(w, "Method is not allowed")
		}
	}
}

func docHandler(collection *mongo.Collection) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		record := shared.NewLogRecord(w, r)
		defer record.Log()
		ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)

		var indent string
		switch r.Header.Get("Accept") {
		case "application/json":
			w.Header().Set("Content-Type", "application/json")
		default:
			w.Header().Set("Content-Type", "text/html")
			indent = "  "
		}

		vars := mux.Vars(r)
		uuid, err := primitive.ObjectIDFromHex(vars["uuid"])
		if err != nil {
			fmt.Fprintf(os.Stderr, "%v\n", err)
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(fmt.Sprintf("%v", err)))
			record.Status = http.StatusInternalServerError
			return
		}

		filter := bson.M{"_id": uuid}
		var result shared.JsonDocument

		switch r.Method {
		case "GET":
			err := collection.FindOne(ctx, filter).Decode(&result)
			if err != nil {
				fmt.Fprintf(os.Stderr, "%v\n", err)
				w.WriteHeader(http.StatusNotFound)
				w.Write([]byte(fmt.Sprintf("%v\n", err)))
				record.Status = http.StatusNotFound
				return
			}

			j, _ := json.MarshalIndent(result, "", indent)
			written, err := w.Write(j)
			if err != nil {
				fmt.Fprintf(os.Stderr, "%v\n", err)
			} else {
				record.ResponseBytes += int64(written)
			}
		case "DELETE":
			err := collection.FindOneAndDelete(ctx, filter).Decode(&result)
			if err != nil {
				fmt.Fprintf(os.Stderr, "%v\n", err)
				w.WriteHeader(http.StatusNotFound)
				w.Write([]byte(fmt.Sprintf("%v\n", err)))
				record.Status = http.StatusNotFound
				return
			}

			_, err = json.MarshalIndent(result, "", indent)
			if err != nil {
				fmt.Fprintf(os.Stderr, "%v\n", err)
				w.WriteHeader(http.StatusInternalServerError)
				w.Write([]byte(fmt.Sprintf("%v\n", err)))
				record.Status = http.StatusInternalServerError
				return
			} else {
				w.WriteHeader(http.StatusNoContent)
				record.Status = http.StatusNoContent
			}
		case "PUT":
			var tmp shared.JsonDocument
			err = json.NewDecoder(r.Body).Decode(&tmp)
			if err != nil {
				fmt.Fprintf(os.Stderr, "%v\n", err)
				w.WriteHeader(http.StatusInternalServerError)
				w.Write([]byte(fmt.Sprintf("%v\n", err)))
				record.Status = http.StatusInternalServerError
				return
			}

			doc, err := shared.ToDoc(tmp)
			if err != nil {
				fmt.Fprintf(os.Stderr, "failed: %s\n", err)
				w.WriteHeader(http.StatusInternalServerError)
				w.Write([]byte(fmt.Sprintf("%v\n", err)))
				record.Status = http.StatusInternalServerError
				return
			}

			update := bson.D{{
				"$set",
				doc,
			}}

			if err != nil {
				fmt.Fprintf(os.Stderr, "%v\n", err)
				w.WriteHeader(http.StatusInternalServerError)
				w.Write([]byte(fmt.Sprintf("%v\n", err)))
				record.Status = http.StatusInternalServerError
				return
			}

			err = collection.FindOneAndUpdate(ctx, filter, update).Decode(&result)
			if err != nil {
				fmt.Fprintf(os.Stderr, "%v\n", err)
				w.WriteHeader(http.StatusInternalServerError)
				w.Write([]byte(fmt.Sprintf("%v\n", err)))
				record.Status = http.StatusInternalServerError
				return
			}

			j, _ := json.MarshalIndent(result, "", indent)
			written, err := w.Write(j)
			if err != nil {
				fmt.Fprintf(os.Stderr, "%v\n", err)
			} else {
				record.ResponseBytes += int64(written)
			}
		default:
			w.WriteHeader(http.StatusMethodNotAllowed)
			record.Status = http.StatusMethodNotAllowed
			fmt.Fprintf(w, "Method is not allowed")
		}
	}
}

func main() {
	shared.HandleSignal()

	mongoURL := "mongodb://localhost:27017"

	url := os.Getenv("MONGO_URL")
	if url != "" {
		mongoURL = url
	}

	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(mongoURL))

	if err != nil {
		log.Fatal(err)
	}

	err = client.Ping(ctx, readpref.Primary())

	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf("Connected to %s MongoDB!\n", mongoURL)

	collection := client.Database("test").Collection("titanic")

	r := mux.NewRouter()
	r.HandleFunc("/people/{uuid}", docHandler(collection))

	http.HandleFunc("/people", docsHandler(collection))
	http.HandleFunc("/healthz", shared.HealthHandler)

	http.Handle("/", r)

	fmt.Fprintln(os.Stderr, "Go!")
	http.ListenAndServe(":8080", nil)
}
