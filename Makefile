APP_NAME=apiserver
DOCKER_REPO=kayrus
TAG=v1

export GOOS=linux
export GOARCH=amd64
export GOPATH:=$(PWD)/gopath

build: deps fmt
	CGO_ENABLED=0 GOPATH=$(PWD):$(GOPATH) go build -o server server

deps:
	mkdir -p gopath/src/
	go get github.com/gorilla/mux go.mongodb.org/mongo-driver/bson go.mongodb.org/mongo-driver/bson/primitive go.mongodb.org/mongo-driver/mongo go.mongodb.org/mongo-driver/mongo/options go.mongodb.org/mongo-driver/mongo/readpref go.mongodb.org/mongo-driver/x/bsonx/bsoncore

buildd:
	docker build -t $(DOCKER_REPO)/$(APP_NAME):$(TAG) -f Dockerfile .

buildd-nc:
	docker build --no-cache -t $(DOCKER_REPO)/$(APP_NAME):$(TAG) -f Dockerfile .

push:
	@echo 'push $(TAG) to $(DOCKER_REPO)/$(APP_NAME)'
	docker push $(DOCKER_REPO)/$(APP_NAME):$(TAG)

build-mongo:
	docker build -t $(DOCKER_REPO)/mongo:$(TAG) -f Dockerfile_mongo .

push-mongo:
	@echo 'push $(TAG) to $(DOCKER_REPO)/mongo'
	docker push $(DOCKER_REPO)/mongo:$(TAG)

fmt:
	find src -name '*.go' -exec go fmt {} \;

all: build buildd push build-mongo push-mongo
